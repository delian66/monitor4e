Monitor and log ICMP connectivity to multiple remote hosts.
===========================================================

The monitor4e executable will seek for a configuration file named 'monitor4e.toml' 
file in the current directory, will read it, and will start periodically 
pinging all the hosts, described in the configuration file.


All the pings, their timings and success/fail status are stored to the monitor4e.db 
SQLite3 file in the current directory.


**NB**: In order to form and send the icmp ping packets, the executable needs 
permission to operate with RAW sockets. You should either always start it with 
sudo ( :-| ), or you can grant it only this permission with:

sudo setcap cap_net_raw=+ep $GOPATH/bin/monitor4e

