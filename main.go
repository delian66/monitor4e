package main

import (
	"fmt"
	"net"
	"sync"
	"time"

	"gitlab.com/delian66/monitor4e/config"
	"gitlab.com/delian66/monitor4e/console"
	"gitlab.com/delian66/monitor4e/pinger"
	"gitlab.com/delian66/monitor4e/recorder"
)

func monitorHost(host config.Host) {
	console.Log(fmt.Sprintf("Start pinging host: %v", host))
	for {
		pingtimes, err := pinger.Pinger(host.Name, int(host.MaxLag))
		if err != nil {
			console.Log(fmt.Sprintf("Error pinging host: %s - %s", host.Name, err))
			if opErr, ok := err.(*net.OpError); ok && !opErr.Temporary() {
				console.Log("Permanent error detected. Aborting.")
				break
			}
			recorder.RecordFailedPing(host.Name, pingtimes)
		} else {
			console.Log(fmt.Sprintf("Ok: send:%8.3fms, recv:%8.3fms,  address: %16s, host: %20s", pingtimes.SendMs, pingtimes.FinishMs, pingtimes.RemoteIP, host.Name))
			recorder.RecordOKPing(host.Name, pingtimes)
		}
		time.Sleep(time.Duration(host.Period) * time.Second)
	}
	console.Log(fmt.Sprintf("Stop pinging host: %v", host))
}

func cleanup() {
	recorder.Close()
	console.Close()
}

func main() {
	defer cleanup()
	console.Log("Start")
	var wg sync.WaitGroup
	for _, host := range config.HostParams.Host {
		wg.Add(1)
		go func(pinghost config.Host) {
			defer wg.Done()
			monitorHost(pinghost)
		}(host)
	}
	wg.Wait()
	console.Log("Done")
}
