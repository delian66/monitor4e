package console

import (
	"log"
	"time"
)

var outputChannel = make(chan string)

func Close() {
	close(outputChannel)
	time.Sleep(time.Duration(1) * time.Millisecond)
}

func Log(s string) {
	outputChannel <- s
}

func showOutputToConsole() {
	//	output logs to the terminal
	for line := range outputChannel {
		log.Println(line)
	}
}

func init() {
	go showOutputToConsole()
}
