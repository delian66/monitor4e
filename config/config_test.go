package config

import (
	"reflect"
	"testing"
)

func TestConfigurationReading(t *testing.T) {
	expected := []Host{
		{Name: "NormalServer", Period: 7, MaxLag: 4},
		{Name: "ServerWithNoMaxLag", Period: 2, MaxLag: 1},
		{Name: "ServerWithNoPeriod", Period: 3, MaxLag: 4},
		{Name: "ServerWithOnlyName", Period: 3, MaxLag: 1},
	}
	if !reflect.DeepEqual(expected, HostParams.Host) {
		t.Fatalf("\n%#v\n!=\n%#v\n", expected, HostParams)
	}

}
