package config

import (
	"log"
	"os"

	"github.com/BurntSushi/toml"
)

type Host struct {
	Name   string
	Period int64
	MaxLag int64
}

type hosts struct {
	Host []Host `toml:"host"`
}

func (h *Host) UnmarshalTOML(p interface{}) error {
	data, _ := p.(map[string]interface{})
	h.Name, _ = data["Name"].(string)
	var valid bool
	if h.Period, valid = data["Period"].(int64); !valid {
		h.Period = 3
	}
	if h.MaxLag, valid = data["MaxLag"].(int64); !valid {
		h.MaxLag = 1
	}
	return nil
}

var HostParams hosts

var tomlFile = "monitor4e.toml"
var example_tomlFile = "example.monitor4e.toml"

func init() {
	if _, err := os.Stat(tomlFile); err != nil {
		log.Fatalf(`
Config file %s is missing from the current directory.
Please, copy the file %s to %s , customize it, then try again.`,
			tomlFile, example_tomlFile, tomlFile)
	}

	if _, err := toml.DecodeFile(tomlFile, &HostParams); err != nil {
		log.Fatal(err)
	}
}
