package recorder

import (
	"log"
	"time"

	"database/sql"
	_ "github.com/mattn/go-sqlite3"

	"gitlab.com/delian66/monitor4e/pinger"
)

type pingRecord struct {
	Hostname string
	Response pinger.PingerResponses
	Status   int
}

var pingRecordChannel = make(chan pingRecord)

func RecordOKPing(hostname string, pingtimes pinger.PingerResponses) {
	pingRecordChannel <- pingRecord{Hostname: hostname, Response: pingtimes, Status: 1}
}
func RecordFailedPing(hostname string, pingtimes pinger.PingerResponses) {
	pingRecordChannel <- pingRecord{Hostname: hostname, Response: pingtimes, Status: 0}
}

func pings2DB_Writer(MyDB *sql.DB) {
	for r := range pingRecordChannel {
		//log.Printf("pings2DB_Writer: r: %v\n", r)
		writePing(r.Status, MyDB, r.Hostname, r.Response)
	}
	MyDB.Close()
}

func Close() {
	close(pingRecordChannel)
}

func writePing(status int, MyDB *sql.DB, hostname string, pingtimes pinger.PingerResponses) {
	//log.Printf("writeSuccessFullPing: %s, %v\n", hostname, pingtimes)
	tx, err := MyDB.Begin()
	if err != nil {
		log.Fatal(err)
	}
	stmt, err := tx.Prepare("INSERT INTO pings ( status, ts, hostname, ip, sendms, finishms ) VALUES (?, ?, ?, ?, ?, ?)")
	if err != nil {
		log.Fatal(err)
	}
	defer stmt.Close()
	_, err = stmt.Exec(status, time.Now().Unix(), hostname, pingtimes.RemoteIP, int(pingtimes.SendMs), int(pingtimes.FinishMs))
	if err != nil {
		log.Fatal(err)
	}
	tx.Commit()
}

func init() {
	MyDB, err := sql.Open("sqlite3", "./monitor4e.db")
	if err != nil {
		log.Fatalf("Error opening the db: %q", err)
	}
	//log.Printf("DB init: %v\n", MyDB)

	sqlStmt := `
        CREATE TABLE pings (
            id INTEGER PRIMARY KEY ASC,
            status INTEGER NOT NULL DEFAULT 0,
            ts TIMESTAMP NOT NULL DEFAULT 0,
            hostname STRING NOT NULL DEFAULT '',
            ip STRING NOT NULL DEFAULT '',
            sendms INTEGER NOT NULL DEFAULT 0,
            finishms INTEGER NOT NULL DEFAULT 0
        );
`
	_, err = MyDB.Exec(sqlStmt)
	if err != nil {
		//log.Fatalf("Create table error: %q: %s\n", err, sqlStmt)
	}
	go pings2DB_Writer(MyDB)
}
